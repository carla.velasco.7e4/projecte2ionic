import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { RootObject } from 'src/app/interfaces/intMovie';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.scss'],
})

export class DetalleComponent implements OnInit {

  movie: RootObject; 
  @Input() id;

  constructor(private modalCtrl: ModalController, private data: DataService) { }

  ngOnInit() {
    this.data.getSingleMovie(this.id).subscribe(
      resp => {
        console.log(resp);
        this.movie = resp;
      }
    );
  }

  regresar(){
    this.modalCtrl.dismiss()
  }

}
